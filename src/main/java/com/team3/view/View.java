package com.team3.view;

public interface View {
    void show();
}
