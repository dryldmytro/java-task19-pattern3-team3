package com.team3.view;

import com.team3.controller.ControllerImpl;
import com.team3.model.bouquet.*;
import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.bouquetdecorator.delivery.impl.DeliveryOutCity;
import com.team3.model.bouquetdecorator.delivery.impl.DeviveryWithinCity;
import com.team3.model.bouquetdecorator.discount.BonusDiscount;
import com.team3.model.bouquetdecorator.discount.GoldDiscount;
import com.team3.model.bouquetdecorator.discount.SocialDiscount;
import com.team3.model.bouquetdecorator.packing.impl.Box;
import com.team3.model.bouquetdecorator.packing.impl.Bucket;
import com.team3.model.event.EventConverter;
import com.team3.model.event.eventtype.Birthday;
import com.team3.model.event.eventtype.Burial;
import com.team3.model.salons.Salon;
import com.team3.model.salons.impl.KyivSalon;
import com.team3.model.salons.impl.LvivSalon;
import com.team3.util.UtilMenu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ViewImpl implements View {
    private ControllerImpl controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static ResourceBundle constant = ResourceBundle.getBundle("constant");
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public ViewImpl() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        start();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    private void start() {
        menu.put("1", constant.getString("1"));
        menu.put("2", constant.getString("2"));
        menu.put("3", constant.getString("3"));
        menu.put("q", constant.getString("q"));
    }

    private void pressButton1() {
        System.out.println("\nWhich salon do you want to create Lviv, Kyiv?");
        try {
            Salon salon = null;
            String salonName = bufferedReader.readLine();
            if (salonName.equalsIgnoreCase("lviv")) {
                salon = new LvivSalon();
                controller.createSalon(salon);
            } else if (salonName.equalsIgnoreCase("kyiv")) {
                salon = new KyivSalon();
                controller.createSalon(salon);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton2() {
        System.out.println("All bouquets!");
        controller.showAllBouquets();
    }

    private void pressButton3() {
        System.out.println("\nSelect bouquet types: \n");
        controller.showAllBouquets();
        try {
            if (controller.getSalon() != null) {
                String type = bufferedReader.readLine();
                BouquetType bouquetType = null;
                Bouquet bouquet = null;
                if (type.equalsIgnoreCase("BOUQUET_RED_ROSES")) {
                    bouquetType = BouquetType.BOUQUET_RED_ROSES;
                    bouquet = new BouquetRedRoses();
                } else if (type.equalsIgnoreCase("BOUQUET_WHITE_ROSES")) {
                    bouquetType = BouquetType.BOUQUET_WHITE_ROSES;
                    bouquet = new BouquetWhiteRoses();
                } else if (type.equalsIgnoreCase("USER_BOUQUET")) {
                    bouquetType = BouquetType.USER_BOUQUET;
                    bouquet = new UserBouquet();
                } else if (type.equalsIgnoreCase("BOUQUET_WHITE_ASTER")) {
                    bouquetType = BouquetType.BOUQUET_WHITE_ASTER;
                    bouquet = new BouquetWhiteAster();
                } else if (type.equalsIgnoreCase("BOUQUET_RED_ASTER")) {
                    bouquetType = BouquetType.BOUQUET_RED_ASTER;
                    bouquet = new BouquetRedAster();
                } else if (type.equalsIgnoreCase("BOUQUET_PINK_ASTER")) {
                    bouquetType = BouquetType.BOUQUET_PINK_ASTER;
                    bouquet = new BouquetPinkAster();
                } else if (type.equalsIgnoreCase("BOUQUET_WHITE_BEGONIA")) {
                    bouquetType = BouquetType.BOUQUET_WHITE_BEGONIA;
                    bouquet = new BouquetWhiteBegonia();
                } else if (type.equalsIgnoreCase("BOUQUET_RED_BEGONIA")) {
                    bouquetType = BouquetType.BOUQUET_RED_BEGONIA;
                    bouquet = new BouquetRedBegonia();
                }
                if (bouquetType != null) {
                    controller.createBouquet(bouquet);
                    controller.getSalon().getBouquet(bouquetType);
                } else {
                    pressButton3();
                }
                System.out.println("\n Your bouquet: \n");
                controller.showAllFlowers();
                pressButton4();
            } else {
                pressButton1();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton4() {
        System.out.println("Select: ");
        System.out.println("1 - delivery;   2 - discount;\n" +
                "3 - packing;  4 - event; " +
                "q - exit");
        try {
            String select = bufferedReader.readLine();
            if (select.equalsIgnoreCase("1")) {
                delivery();
            } else if (select.equalsIgnoreCase("2")) {
                discount();
            } else if (select.equalsIgnoreCase("3")) {
                packing();
            } else if (select.equalsIgnoreCase("4")) {
                event();
            } else if (select.equalsIgnoreCase("q")) {
                show();
            }
            pressButton4();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void delivery() {
        System.out.println("\nSelect: \n" +
                "1 - delevery out city;  2 - delivery within city;");
        try {
            String select = bufferedReader.readLine();
            BouquetDecorator bouquetDecorator = null;
            if (select.equalsIgnoreCase("1")) {
                bouquetDecorator = new DeliveryOutCity();
            } else if (select.equalsIgnoreCase("2")) {
                bouquetDecorator = new DeviveryWithinCity();
            } else {
                delivery();
            }
            bouquetDecorator.setBouquet(controller.getBouquetDecorator());
            controller.setBouquetDecorator(bouquetDecorator);
            System.out.println(controller.getBouquetDecorator().getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void discount() {
        System.out.println("\nSelect: \n" +
                "1 - bonus discount;  2 - gold discount;" +
                "3 - social discount");
        try {
            String select = bufferedReader.readLine();
            BouquetDecorator bouquetDecorator = null;
            if (select.equalsIgnoreCase("1")) {
                bouquetDecorator = new BonusDiscount();
            } else if (select.equalsIgnoreCase("2")) {
                bouquetDecorator = new GoldDiscount();
            } else if (select.equalsIgnoreCase("3")) {
                bouquetDecorator = new SocialDiscount();
            } else {
                discount();
            }
            bouquetDecorator.setBouquet(controller.getBouquetDecorator());
            controller.setBouquetDecorator(bouquetDecorator);
            System.out.println(controller.getBouquetDecorator().getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void packing() {
        System.out.println("\nSelect: \n" +
                "1 - box packing;  2 - bucket packing;");
        try {
            String select = bufferedReader.readLine();
            BouquetDecorator bouquetDecorator = null;
            if (select.equalsIgnoreCase("1")) {
                bouquetDecorator = new Box();
            } else if (select.equalsIgnoreCase("2")) {
                bouquetDecorator = new Bucket();
            } else {
                packing();
            }
            bouquetDecorator.setBouquet(controller.getBouquetDecorator());
            controller.setBouquetDecorator(bouquetDecorator);
            System.out.println(controller.getBouquetDecorator().getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void event() {
        System.out.println("\nSelect event\n" +
                "1 - Birthday;  2 - Burial");
        try {
            String select = bufferedReader.readLine();
            EventConverter eventConverter = null;
            if (select.equalsIgnoreCase("1")) {
                eventConverter = new Birthday(controller.getBouquetDecorator());
            } else if (select.equalsIgnoreCase("2")) {
                eventConverter = new Burial(controller.getBouquetDecorator());
            } else {
                event();
            }
            System.out.println(controller.getBouquetDecorator().getName());
            System.out.println("Flowers: " + controller.getBouquetDecorator().getFlowers().size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void show() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
