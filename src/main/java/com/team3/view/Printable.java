package com.team3.view;

@FunctionalInterface
public interface Printable {
    void print();
}
