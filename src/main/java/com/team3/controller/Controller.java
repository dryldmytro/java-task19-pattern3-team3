package com.team3.controller;

import com.team3.model.bouquet.Bouquet;
import com.team3.model.salons.Salon;

public interface Controller {
    void createSalon(Salon salon);

    Salon getSalon();

    void createBouquet(Bouquet bouquet);

    Bouquet getBouquet();

    void showAllBouquets();
}
