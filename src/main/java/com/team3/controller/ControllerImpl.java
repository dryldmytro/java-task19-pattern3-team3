package com.team3.controller;

import com.team3.model.bouquet.Bouquet;
import com.team3.model.bouquet.BouquetType;
import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.salons.Salon;

import java.util.stream.Stream;

public class ControllerImpl {
    private Salon salon;
    private Bouquet bouquet;
    private BouquetDecorator bouquetDecorator = new BouquetDecorator();

    public void createSalon(Salon salon) {
        this.salon = salon;
    }

    public void createBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
        bouquetDecorator.setBouquet(bouquet);
    }

    public Salon getSalon() {
        return salon;
    }

    public Bouquet getBouquet() {
        return bouquet;
    }

    public void showAllBouquets() {
        BouquetType[] values = BouquetType.values();
        Stream.of(values).forEach(System.out::println);
    }

    public void showAllFlowers() {
        bouquet.getFlowers().forEach(System.out::println);
    }

    public void setBouquetDecorator(BouquetDecorator bouquetDecorator) {
        this.bouquetDecorator = bouquetDecorator;
    }

    public BouquetDecorator getBouquetDecorator() {
        return bouquetDecorator;
    }
}
