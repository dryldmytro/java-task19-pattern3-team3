package com.team3;

import com.team3.view.ViewImpl;

public class App {
    public static void main(String[] args) {
        new ViewImpl().show();
    }
}
