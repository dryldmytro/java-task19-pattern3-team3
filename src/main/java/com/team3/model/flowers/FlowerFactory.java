package com.team3.model.flowers;

import com.team3.model.flowers.impl.*;

public class FlowerFactory {
    public static Flower createFlower(FlowerType type) {
        Flower flower = null;
        if (type == FlowerType.RED_ROSE) {
            flower = new RedRose();
        } else if (type == FlowerType.WHITE_ROSE) {
            flower = new WhiteRose();
        } else if (type == FlowerType.PINK_ASTER) {
            flower = new PinkAster();
        } else if (type == FlowerType.RED_ASTER) {
            flower = new RedAster();
        } else if (type == FlowerType.RED_BEGONIA) {
            flower = new RedBegonia();
        } else if (type == FlowerType.WHITE_ASTER) {
            flower = new WhiteAster();
        } else if (type == FlowerType.WHITE_BEGONIA) {
            flower = new WhiteBegonia();
        }
        return flower;
    }
}
