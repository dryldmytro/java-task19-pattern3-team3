package com.team3.model.flowers;

public enum FlowerType {
    RED_ROSE, WHITE_ROSE, WHITE_ASTER, PINK_ASTER, RED_ASTER, RED_BEGONIA, WHITE_BEGONIA
}
