package com.team3.model.flowers.impl;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.flowers.Flower;

public class WhiteBegonia extends BouquetDecorator implements Flower {
    private final double PRICE = 13;
    private final String NAME = "White Begonia";

    public WhiteBegonia() {
        setComponentName(getFlowerName());
        setComponentPrice(getPrice());
        setFlower(this);
    }


    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public String getFlowerName() {
        return NAME;
    }

    @Override
    public String toString() {
        return "WhiteBegonia{" +
                "PRICE=" + PRICE +
                ", NAME='" + NAME + '\'' +
                '}';
    }
}
