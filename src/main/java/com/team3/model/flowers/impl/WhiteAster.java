package com.team3.model.flowers.impl;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.flowers.Flower;

public class WhiteAster extends BouquetDecorator implements Flower {
    private final double PRICE = 20;
    private final String NAME = "White Aster";

    public WhiteAster() {
        setComponentName(getFlowerName());
        setComponentPrice(getPrice());
        setFlower(this);
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public String getFlowerName() {
        return NAME;
    }

    @Override
    public String toString() {
        return "WhiteAster{" +
                "PRICE=" + PRICE +
                ", NAME='" + NAME + '\'' +
                '}';
    }
}
