package com.team3.model.flowers.impl;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.flowers.Flower;

public class RedRose extends BouquetDecorator implements Flower {
  private final double PRICE = 15;
  private final String NAME = "Red Rose";
  public RedRose() {
    setComponentName(getFlowerName());
    setComponentPrice(getPrice());
    setFlower(this);
  }

  @Override
  public double getPrice() {
    return PRICE;
  }

  @Override
  public String getFlowerName() {
    return NAME;
  }

  @Override
  public String toString() {
    return "RedRose{" +
            "PRICE=" + PRICE +
            ", NAME='" + NAME + '\'' +
            '}';
  }
}
