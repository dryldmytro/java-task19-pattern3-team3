package com.team3.model.flowers.impl;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.flowers.Flower;

public class RedAster extends BouquetDecorator implements Flower {
    private final double PRICE = 16;
    private final String NAME = "Red Aster";

    public RedAster() {
        setComponentName(getFlowerName());
        setComponentPrice(getPrice());
        setFlower(this);
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public String getFlowerName() {
        return NAME;
    }

    @Override
    public String toString() {
        return "RedAster{" +
                "PRICE=" + PRICE +
                ", NAME='" + NAME + '\'' +
                '}';
    }
}
