package com.team3.model.flowers.impl;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.flowers.Flower;

public class RedBegonia extends BouquetDecorator implements Flower {
    private final double PRICE = 12;
    private final String NAME = "Red Begonia";

    public RedBegonia() {
        setComponentName(getFlowerName());
        setComponentPrice(getPrice());
        setFlower(this);
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public String getFlowerName() {
        return NAME;
    }

    @Override
    public String toString() {
        return "RedBegonia{" +
                "PRICE=" + PRICE +
                ", NAME='" + NAME + '\'' +
                '}';
    }
}
