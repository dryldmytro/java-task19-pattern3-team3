package com.team3.model.flowers.impl;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.flowers.Flower;

public class WhiteRose extends BouquetDecorator implements Flower {
    private final double PRICE = 15;
    private final String NAME = "White Rose";

    public WhiteRose() {
        setComponentName(getFlowerName());
        setComponentPrice(getPrice());
        setFlower(this);
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public String getFlowerName() {
        return NAME;
    }

    @Override
    public String toString() {
        return "WhiteRose{" +
                "PRICE=" + PRICE +
                ", NAME='" + NAME + '\'' +
                '}';
    }
}
