package com.team3.model.flowers.impl;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.flowers.Flower;

public class PinkAster extends BouquetDecorator implements Flower {
    private final double PRICE = 18;
    private final String NAME = "Pink Aster";

    public PinkAster() {
        setComponentName(getFlowerName());
        setComponentPrice(getPrice());
        setFlower(this);
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public String getFlowerName() {
        return NAME;
    }

    @Override
    public String toString() {
        return "PinkAster{" +
                "PRICE=" + PRICE +
                ", NAME='" + NAME + '\'' +
                '}';
    }
}
