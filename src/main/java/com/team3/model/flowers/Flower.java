package com.team3.model.flowers;

public interface Flower {
    double getPrice();

    String getFlowerName();
}
