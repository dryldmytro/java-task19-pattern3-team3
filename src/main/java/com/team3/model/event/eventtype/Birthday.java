package com.team3.model.event.eventtype;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.event.EventConverter;

public class Birthday extends EventConverter {
    private final String congratulations = "Happy birthday";

    public Birthday(BouquetDecorator bouquet) {
        super(bouquet);
        addString();
    }

    public void addString() {
        getBouquetDecorator().setComponentName(congratulations);
    }
}
