package com.team3.model.event.eventtype;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.event.EventConverter;

public class Burial extends EventConverter {
    private final String sorry = "Im so sorry";

    public Burial(BouquetDecorator bouquet) {
        super(bouquet);
        checkSize();
        addString();
    }

    public void checkSize() {
        int size = getBouquetDecorator().getFlowers().size();
        if (size % 2 != 0) {
            deleteOneElement();
        }
    }

    public void deleteOneElement() {
        getBouquetDecorator().getFlowers().remove(0);
    }

    public void addString() {
        getBouquetDecorator().setComponentName(sorry);
    }

}
