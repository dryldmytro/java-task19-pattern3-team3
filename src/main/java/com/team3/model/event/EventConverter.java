package com.team3.model.event;

import com.team3.model.bouquetdecorator.BouquetDecorator;

public abstract class EventConverter {
    private BouquetDecorator bouquetDecorator;

    public EventConverter(BouquetDecorator bouquet) {
        this.bouquetDecorator = bouquet;
    }

    public BouquetDecorator getBouquetDecorator() {
        return bouquetDecorator;
    }
}
