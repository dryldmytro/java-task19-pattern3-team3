package com.team3.model.bouquet;

import com.team3.model.flowers.Flower;

import java.util.List;

public interface Bouquet {
    double getBouquetPrice();

    String getName();

    List<Flower> getFlowers();
}
