package com.team3.model.bouquet;

import com.team3.model.flowers.Flower;

import java.util.ArrayList;
import java.util.List;

public class UserBouquet implements Bouquet {
    private double bouquetPrice;
    private List<Flower> flowers;
    final private String NAME = "UserBouquet";

    public UserBouquet() {
        flowers = new ArrayList<>();
        setBouquetPrice(bouquetPrice);
    }

    public void setBouquetPrice(double price) {
        for (Flower f : getFlowers()) {
            price += f.getPrice();
        }
        this.bouquetPrice = price;
    }

    @Override
    public double getBouquetPrice() {
        return bouquetPrice;
    }

    @Override
    public String getName() {
        return NAME;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    @Override
    public String toString() {
        return "UserBouquet{" +
                "flowers=" + flowers +
                '}';
    }
}
