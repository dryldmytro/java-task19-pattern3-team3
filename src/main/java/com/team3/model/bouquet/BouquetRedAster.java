package com.team3.model.bouquet;

import com.team3.model.flowers.Flower;
import com.team3.model.flowers.FlowerFactory;
import com.team3.model.flowers.FlowerType;

import java.util.ArrayList;
import java.util.List;

public class BouquetRedAster implements Bouquet {
    private double bouquetPrice;
    private List<Flower> flowers;
    final private String NAME = "BouquetRedAster";

    public BouquetRedAster() {
        flowers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            flowers.add(FlowerFactory.createFlower(FlowerType.RED_ASTER));
        }
        setBouquetPrice(bouquetPrice);
    }

    public void setBouquetPrice(double price) {
        for (Flower f : getFlowers()) {
            price += f.getPrice();
        }
        this.bouquetPrice = price;
    }

    @Override
    public double getBouquetPrice() {
        return bouquetPrice;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public List<Flower> getFlowers() {
        return flowers;
    }

    @Override
    public String toString() {
        return "BouquetRedAster{" +
                "flowers=" + flowers +
                '}';
    }
}
