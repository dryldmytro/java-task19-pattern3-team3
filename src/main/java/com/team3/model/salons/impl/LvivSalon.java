package com.team3.model.salons.impl;

import com.team3.model.bouquet.*;
import com.team3.model.salons.Salon;

public class LvivSalon extends Salon {
    @Override
    protected Bouquet createBouquet(BouquetType type) {
        Bouquet bouquet = null;
        if (type == BouquetType.BOUQUET_WHITE_ROSES) {
            bouquet = new BouquetRedRoses();
        } else if (type == BouquetType.USER_BOUQUET) {
            bouquet = new UserBouquet();
        } else if (type == BouquetType.BOUQUET_WHITE_BEGONIA) {
            bouquet = new BouquetWhiteBegonia();
        } else if (type == BouquetType.BOUQUET_RED_BEGONIA) {
            bouquet = new BouquetRedBegonia();
        } else if (type == BouquetType.BOUQUET_PINK_ASTER) {
            bouquet = new BouquetPinkAster();
        }
        return bouquet;
    }
}
