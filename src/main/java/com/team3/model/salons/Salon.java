package com.team3.model.salons;

import com.team3.model.bouquet.Bouquet;
import com.team3.model.bouquet.BouquetType;

public abstract class Salon {
    protected abstract Bouquet createBouquet(BouquetType type);

    public Bouquet getBouquet(BouquetType type) {
        Bouquet bouquet = createBouquet(type);
        return bouquet;
    }
}
