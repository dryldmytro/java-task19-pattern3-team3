package com.team3.model.bouquetdecorator.discount;

public interface Discount {
    double getDiscount();

    String getDiscountName();
}
