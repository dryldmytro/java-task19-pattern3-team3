package com.team3.model.bouquetdecorator.discount;

import com.team3.model.bouquetdecorator.BouquetDecorator;

public class BonusDiscount extends BouquetDecorator implements Discount {
    private final double DISCOUNT = -25;
    private final String NAME = "BonusDiscount";

    public BonusDiscount() {
        setComponentName(getDiscountName());
        setComponentPrice(getDiscount());
    }

    @Override
    public double getDiscount() {
        return DISCOUNT;
    }

    @Override
    public String getDiscountName() {
        return NAME;
    }
}
