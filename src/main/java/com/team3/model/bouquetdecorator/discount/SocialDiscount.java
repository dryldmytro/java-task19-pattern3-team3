package com.team3.model.bouquetdecorator.discount;

import com.team3.model.bouquetdecorator.BouquetDecorator;

public class SocialDiscount extends BouquetDecorator implements Discount {

    private final double DISCOUNT = -10;
    private final String NAME = "SocialDiscount";

    public SocialDiscount() {
        setComponentName(getDiscountName());
        setComponentPrice(getDiscount());
    }

    @Override
    public double getDiscount() {
        return DISCOUNT;
    }

    @Override
    public String getDiscountName() {
        return NAME;
    }
}
