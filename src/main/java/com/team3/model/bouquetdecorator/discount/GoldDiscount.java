package com.team3.model.bouquetdecorator.discount;

import com.team3.model.bouquetdecorator.BouquetDecorator;

public class GoldDiscount extends BouquetDecorator implements Discount {

    private final double DISCOUNT = -50;
    private final String NAME = "GoldDiscount";

    public GoldDiscount() {
        setComponentName(getDiscountName());
        setComponentPrice(getDiscount());
    }

    @Override
    public double getDiscount() {
        return DISCOUNT;
    }

    @Override
    public String getDiscountName() {
        return NAME;
    }
}
