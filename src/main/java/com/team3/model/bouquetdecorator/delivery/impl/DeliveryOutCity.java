package com.team3.model.bouquetdecorator.delivery.impl;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.bouquetdecorator.delivery.Delivery;

public class DeliveryOutCity extends BouquetDecorator implements Delivery {
    private final double PRICE = 50;
    private final String NAME = "Delivery out city";

    public DeliveryOutCity() {
        setComponentName(getDeliveryName());
        setComponentPrice(getPrice());
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public String getDeliveryName() {
        return NAME;
    }
}
