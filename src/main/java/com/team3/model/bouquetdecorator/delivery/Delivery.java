package com.team3.model.bouquetdecorator.delivery;

public interface Delivery {
    double getPrice();

    String getDeliveryName();
}
