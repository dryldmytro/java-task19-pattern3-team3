package com.team3.model.bouquetdecorator.delivery.impl;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.bouquetdecorator.delivery.Delivery;

public class DeviveryWithinCity extends BouquetDecorator implements Delivery {
    private final double PRICE = 40;
    private final String NAME = "Delivery within the city";

    public DeviveryWithinCity() {
        setComponentName(getDeliveryName());
        setComponentPrice(getPrice());
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public String getDeliveryName() {
        return NAME;
    }
}
