package com.team3.model.bouquetdecorator;

import com.team3.model.bouquet.Bouquet;
import com.team3.model.flowers.Flower;

import java.util.List;
import java.util.Optional;

public class BouquetDecorator implements Bouquet {
    private Optional<Bouquet> bouquet;
    private String componentName = "";
    private Flower flower;
    private double componentPrice;

    public void setBouquet(Bouquet outBouquet) {
        bouquet = Optional.ofNullable(outBouquet);
        if (flower != null) {
            bouquet.orElseThrow(IllegalArgumentException::new).getFlowers().add(flower);
        }
    }

    public void setFlower(Flower flower) {
        this.flower = flower;
    }


    public void setComponentPrice(double componentPrice) {
        this.componentPrice = componentPrice;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    @Override
    public double getBouquetPrice() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getBouquetPrice() + componentPrice;
    }

    @Override
    public String getName() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getName() + " +" + componentName;
    }

    @Override
    public List<Flower> getFlowers() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getFlowers();
    }
}
