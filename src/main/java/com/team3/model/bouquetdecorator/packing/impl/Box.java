package com.team3.model.bouquetdecorator.packing.impl;


import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.bouquetdecorator.packing.Packing;

public class Box extends BouquetDecorator implements Packing {
    private final double PRICE = 75;
    private final String NAME = "Box";

    public Box() {
        setComponentName(getPackingName());
        setComponentPrice(getPrice());
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public String getPackingName() {
        return NAME;
    }
}

