package com.team3.model.bouquetdecorator.packing.impl;

import com.team3.model.bouquetdecorator.BouquetDecorator;
import com.team3.model.bouquetdecorator.packing.Packing;

public class Bucket extends BouquetDecorator implements Packing {
    private final double PRICE = 45;
    private final String NAME = "Bucket";

    public Bucket() {
        setComponentName(getPackingName());
        setComponentPrice(getPrice());
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public String getPackingName() {
        return NAME;
    }
}
