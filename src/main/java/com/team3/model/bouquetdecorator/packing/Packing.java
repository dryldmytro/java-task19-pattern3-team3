package com.team3.model.bouquetdecorator.packing;

public interface Packing {
    double getPrice();

    String getPackingName();
}
